#
# SphericalHarmonics/Makefile
#
# Build script for the SphericalHarmonics
#


CC  ?= $(shell root-config --cc)
CXX ?= $(shell root-config --cxx)

ROOT_CFLAGS ?= $(shell root-config --cflags)
ROOT_LIBS ?= $(shell root-config --libs)

ifndef CXXFLAGS
  CXXFLAGS := $(shell gsl-config --cflags) $(ROOT_CFLAGS)
endif

ifndef LDFLAGS
  LDFLAGS := $(shell gsl-config --libs) $(ROOT_LIBS)
endif

BUILD_MODE ?= "debug"

ifeq (${BUILD_MODE}, optimized)
  OPTIMIZATION := -O3
else ifeq (${BUILD_MODE}, debug)
  OPTIMIZATION := -O0 -g
else
  OPTIMIZATION := -O2 -g
endif

# creates the dependency file next to object file
DEPEND = -MMD -MP -MF $(patsubst %.o,%.d,$@)

CXXFLAGS := ${OPTIMIZATION} ${CXXFLAGS}
LDFLAGS := ${OPTIMIZATION} ${LDFLAGS}

.PHONY: all clean


all: testylmgsl

testylmgsl: testylmgsl.o ylm.o mmatrix.o
	${CXX} -o $@ $^ $(LDFLAGS)

test: test.o ylm.o mmatrix.o
	${CXX} -o $@ $^ $(LDFLAGS)

testgslm: testgslm.o
	${CXX} -o $@ $^ $(LDFLAGS)

testcg: testcg.o mmatrix.o
	${CXX} -o $@ $^ $(LDFLAGS)

testylm: testylm.o ylm.o
	${CXX} -o $@ $^ $(LDFLAGS)

CorrFctnDirectYlm.o: CorrFctnDirectYlm.cxx CorrFctnDirectYlm.h
	$(CXX) $< -o $@ $(CXXFLAGS) -c

testclass: testclass.o CorrFctnDirectYlm.o ylm.o
	${CXX} -o $@ $^ $(LDFLAGS)

testclassread: testclassread.o CorrFctnDirectYlm.o ylm.o
	${CXX} -o $@ $^ $(LDFLAGS)

testinteg: testinteg.o CorrFctnDirectYlm.o ylm.o
	${CXX} -o $@ $^ $(LDFLAGS)

testcfcalc: testcfcalc.o ylm.o
	${CXX} -o $@ $^ $(LDFLAGS)

integpion: integpion.o ylm.o CorrFctnDirectYlm.o
	${CXX} -o $@ $^ $(LDFLAGS)


%.o: %.cc
	$(CXX) -c $(CXXFLAGS) $(DEPEND) $< -o $@

%.o: %.cxx
	$(CXX) -c $(CXXFLAGS) $(DEPEND) $< -o $@


clean:
	rm -f testylm testylmgsl testcg testylm *.o *.d

ifneq ($(MAKECMDGOALS), clean)
-include $(patsubst %.cxx,%.d,$(wildcard *.cxx)) $(patsubst %.cc,%.d,$(wildcard *.cc))
endif
