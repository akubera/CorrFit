#
# Makefile
#
# Main makefile for CorrFit
#

CC  = $(shell root-config --cc)
CXX = $(shell root-config --cxx)
FF  = $(shell root-config --f77)

# Set to debug | optimized to change compiler flags
BUILD_MODE ?= "debug"

# Determine if compiler is clang based
ifneq (,$(shell ${CXX} -dM -E -x c /dev/null | grep __clang__))
 COMPILER_IS_CLANG = 1
 COMPILER_IS_GNU = 0
else
 # assume the only other compiler is GNU
 COMPILER_IS_CLANG = 0
 COMPILER_IS_GNU = 1
endif

# Get ROOT compiler flags
ROOT_CFLAGS := $(shell root-config --cflags)
ROOT_LIBS := $(shell root-config --libs)

# Explicitly locate gfortran's shared or static library
# - this is required on some systems for some reason...
FORTRAN_LIB := $(shell gcc -print-file-name=libgfortran.a)
ifeq (libgfortran.a,${FORTRAN_LIB})     # libgfortran.a not found
  FORTRAN_LIB := $(shell gcc -print-file-name=libgfortran.so)
ifeq (libgfortran.so,${FORTRAN_LIB})    # libgfortran.so not found
  FORTRAN_LIB := $(shell gcc -print-file-name=libgfortran.dylib)
ifeq (libgfortran.dylib,${FORTRAN_LIB}) # libgfortran.dylib not found
  $(error Could not find libgfortran. Is it installed?)
endif
endif
endif
ROOT_LIBS += -L$(shell dirname ${FORTRAN_LIB})


# if not clang - remove any stray -stdlib configurations (...it happens sometimes...)
ifeq (${COMPILER_IS_CLANG}, 0)
 ROOT_CFLAGS := $(subst $(filter -stdlib=%,${ROOT_CFLAGS}),,${ROOT_CFLAGS})
 ROOT_LIBS := $(subst $(filter -stdlib=%,${ROOT_LIBS}),,${ROOT_LIBS})
endif


export CC CXX FF BUILD_MODE ROOT_CFLAGS ROOT_LIBS

.PHONY: all doc-server clean CorrFit SphericalHarmonics optimized

all: SphericalHarmonics CorrFit
	@echo
	@echo "😃   Finished Building CorrFit!"
	@echo

docs: */**.h */**.cxx */**.cc */**.F
	doxygen

doc-server: docs
	cd docs/html && python3 -m http.server

CorrFit:
	# cd CorrFit && $(MAKE) MAKEFLAGS=
	+make -C CorrFit bin/GetMin.exe bin/CompareWeights.exe

SphericalHarmonics:
	+make -C SphericalHarmonics

optimized:
	BUILD_MODE=optimized $(MAKE) all

clean:
	make -C CorrFit clean
	make -C SphericalHarmonics clean
