///
/// \file SourceModel.h
///

#pragma once

#ifndef _CORRFIT_SOURCEMODEL_H_
#define _CORRFIT_SOURCEMODEL_H_

#include "CFGlobal.h"

/// \class SourceModel
/// \brief Abstract base class of source models
///
class SourceModel {
public:
  ///
  SourceModel();
  /// Construct with number of parameters and number of random variables
  SourceModel(int, int);

  virtual void        SetModelParameters(double *aParTable) = 0;
  virtual double*     GetModelParameters() = 0;
  virtual void        GeneratePos(const VectorPair *aMom, VectorPair *aPos, double *aRandVar) = 0;
  virtual int         GetNParameters() const;
  virtual int         GetNRandVar() const;
  virtual void        InitRandVar(double** aRandVar) = 0;
  virtual const char* GetParameterName(int aPar) = 0;
  virtual const char* GetModelIdentifier() = 0;

protected:
  int mNPar;
  int mNRandVar;
};


inline
SourceModel::SourceModel()
{
}

inline
int
SourceModel::GetNParameters() const
{
  return mNPar;
}

inline
int
SourceModel::GetNRandVar() const
{
  return mNRandVar;
}


inline
SourceModel::SourceModel(int n_par, int n_randvar):
  mNPar(n_par),
  mNRandVar(n_randvar)
{}

#endif
