///
/// \file CF.h
///

#pragma once

#ifndef _CORRFIT_CF_H_
#define _CORRFIT_CF_H_


/// \class CF
/// \brief Abstract Base Class of all correlation functions
///
/// This class defines the members mContent and mErr2 but
/// does NOT own them, they will have to be deleted/freed
/// from elsewhere.
///
class CF {
public:

  /// Constructor
  CF();

  virtual void Write() = 0;
  //  virtual void Read(TFile *aFile, TKey *aKey) = 0;
  virtual void Normalize() = 0;

  virtual double* GetContent();
  virtual double* GetError2();
  virtual const double* GetContent() const;
  virtual const double* GetError2() const;

protected:
  double *mContent;
  double *mErr2;
};


inline
CF::CF():
  mContent(nullptr)
, mErr2(nullptr)
{
}

inline
double*
CF::GetContent()
{
  return mContent;
}

inline
double*
CF::GetError2()
{
  return mErr2;
}

inline
const double*
CF::GetContent() const
{
  return mContent;
}

inline
const double*
CF::GetError2() const
{
  return mErr2;
}



#endif
