///
/// \file SourceModelExpGausRInv.h
///

#pragma once

#ifndef _CORRFIT_SOURCEMODELEXPGAUSRINV_H_
#define _CORRFIT_SOURCEMODELEXPGAUSRINV_H_

#include "CFGlobal.h"
#include "SourceModel.h"

#include <array>

class TRandom;


/// \class SourceModelExpGausRInv
///
class SourceModelExpGausRInv : public SourceModel {
public:
  SourceModelExpGausRInv();
  ~SourceModelExpGausRInv();

  virtual void        SetModelParameters(double *aParTable);
  virtual double*     GetModelParameters();
  virtual void        GeneratePos(const VectorPair *aMom, VectorPair *aPos, double *aRandVar);
  virtual const char* GetParameterName(int aPar);
  virtual void        InitRandVar(double** aRandVar);
  virtual const char* GetModelIdentifier();

protected:
  double GetXFromY(double aY, double aPar);
  double FunExp(double aArg, double aPar);
  std::array<double, 3> mParameters;
  double mExpProbability;
  static TRandom *mRandom;
};

inline
double*
SourceModelExpGausRInv::GetModelParameters()
{
  return &mParameters[0];
}


#endif
