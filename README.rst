=======
CorrFit
=======

Femtoscopic correlation fitting framework.

For more information, see the reference_.


This repository is designed to make it easy for people to find and install
the CorrFit project.

As of 2016, the project has stagnated and the build system no longer works,
any efforts to fix the Makefiles would be appreciated.


Installation
------------

A simple ``make`` command will build the ``SphericalHarmonics`` and ``CorrFit``
projects.

A mysql database database will need to be created to hold information. Use
the ``CFStorage.structure.sql`` file to initialize the tables in the database.


License
-------

This software is authored by Adam Kisiel, use of the software for academic
works should cite the presentation of the software:

.. code::

    A. Kisiel, "CorrFit - a program to fit arbitrary two-particle correlation functions"
               in 2nd Warsaw Meeting On Particle Correlations and Resonannces in Heavy Ion
               Collisions, Nukleonika Conference Proceedings, 49 suppl. 2, pp.81-83 (2004).

.. _reference: https://inis.iaea.org/search/search.aspx?orig_q=RN:36067768
